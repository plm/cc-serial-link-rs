use cc_serial_link::{
    parser::*,
    protocol::{ttl::*, *},
    serializer::*,
};

const SAMPLE_COMMAND_PACKET: [u8; 5] = [0x8A, 0x1, 0x0, 0x0, 0x75];

#[test]
fn parse_full_command_packet() {
    if let Ok(command) = TTLSerialCommand::parse_packet(&SAMPLE_COMMAND_PACKET[..]) {
        assert_eq!(command.device, DeviceID::try_from(10).unwrap());
        assert_eq!(command.command, Command::ReadRegister(ReadRegister::Ripple));
    } else {
        unreachable!()
    }
}

#[test]
fn parse_truncated_command_packet() {
    for len in 1..SAMPLE_COMMAND_PACKET.len() - 1 {
        assert!(TTLSerialCommand::parse_packet(&SAMPLE_COMMAND_PACKET[..len]).is_err());
    }
}

const SAMPLE_RESPONSE_PACKET: [u8; 3] = [0xFF, 0xFF, 0x2];

#[test]
fn parse_full_response_packet() {
    if let Ok(command) = TTLSerialResponse::parse_packet(&SAMPLE_RESPONSE_PACKET[..]) {
        assert_eq!(command.data, ResponseData(0xFFFF));
    } else {
        unreachable!()
    }
}

#[test]
fn parse_truncated_response_packet() {
    for len in 1..SAMPLE_RESPONSE_PACKET.len() - 1 {
        assert!(TTLSerialResponse::parse_packet(&SAMPLE_RESPONSE_PACKET[..len]).is_err());
    }
}

const SAMPLE_CLEAR_PACKET: [u8; 5] = [0x0; 5];

#[test]
fn parse_full_clear_packet() {
    assert_eq!(
        TTLSerialClear::parse_packet(&SAMPLE_CLEAR_PACKET[..]),
        Ok(TTLSerialClear)
    );
}

#[test]
fn parse_truncated_clear_packet() {
    for len in 1..SAMPLE_RESPONSE_PACKET.len() - 1 {
        assert!(TTLSerialClear::parse_packet(&SAMPLE_RESPONSE_PACKET[..len]).is_err());
    }
}

#[test]
fn serialize_register_parse_command() {
    let packet = TTLSerialCommand::new(
        DeviceID::try_from(10).unwrap(),
        Command::ReadRegister(ReadRegister::Current),
    );
    let mut buffer = [0xFF; 6];
    if let Ok(written) = packet.serialize_packet(&mut buffer) {
        assert_eq!(written.len(), 5);
        assert_eq!(written[0], 0x8A);
        assert_eq!(written[1], 0x2);
        assert_eq!(written[2], 0x0);
        assert_eq!(written[3], 0x0);
        assert_eq!(written[4], 0x74);
    } else {
        unreachable!()
    }
}

#[test]
fn serialize_write_register_command() {
    let packet = TTLSerialCommand::new(
        DeviceID::try_from(1).unwrap(),
        Command::WriteRegister(WriteRegister::Throttle, CommandData(50)),
    );
    let mut buffer = [0xFF; 6];
    if let Ok(written) = packet.serialize_packet(&mut buffer) {
        assert_eq!(written.len(), 5);
        assert_eq!(written[0], 0x81);
        assert_eq!(written[1], 0x80);
        assert_eq!(written[2], 0x32);
        assert_eq!(written[3], 0x0);
        assert_eq!(written[4], 0xCD);
    } else {
        unreachable!()
    }
    assert_eq!(buffer[5], 0xFF);
}

#[test]
fn serialize_response() {
    let packet = TTLSerialResponse::new(ResponseData(100));
    let mut buffer = [0xFF; 4];
    if let Ok(written) = packet.serialize_packet(&mut buffer) {
        assert_eq!(written.len(), 3);
        assert_eq!(written[0], 0x64);
        assert_eq!(written[1], 0x0);
        assert_eq!(written[2], 0x9C);
    } else {
        unreachable!()
    }
    assert_eq!(buffer[3], 0xFF);
}

#[test]
fn serialize_clear() {
    let packet = TTLSerialClear;
    let mut buffer = [0xFF; 6];
    if let Ok(written) = packet.serialize_packet(&mut buffer) {
        assert_eq!(written.len(), 5);
        assert_eq!(written, &[0x0; 5]);
    } else {
        unreachable!()
    }
    assert_eq!(buffer[5], 0xFF);
}
