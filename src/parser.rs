pub mod ttl;

#[derive(Debug, PartialEq, Eq)]
pub struct ParserError;

pub type Input<'i> = &'i [u8];
pub type ParserResult<'i, P> = Result<P, ParserError>;

pub trait PacketParser<P> {
    fn parse_packet(input: Input) -> ParserResult<P>;
}
