pub mod convert;
pub mod register;
pub mod ttl;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Register {
    Read(ReadRegister),
    Write(WriteRegister),
    Reserved(Reserved),
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum ReadRegister {
    /// The controller's input voltage
    Voltage,
    /// The controller's input voltage ripple
    Ripple,
    /// The controller’s current draw
    Current,
    /// The controller’s commanded throttle value
    Throttle,
    /// The controller's output throttle percentage
    Power,
    /// The motor's electrical RPM
    Speed,
    /// The controller's temperature
    Temp,
    /// The BEC's voltage
    BECVolt,
    /// The BEC's current load
    BECCurrent,
    /// The raw NTC temperature value
    RawNTC,
    /// The raw linear temperature value
    RawLinear,
    /// Whether the serial device is in Link Live mode
    LinkLive,
    /// The E. Stop/RX Glitch fail safe output (0 = 1ms; 100 = 2ms)
    FailSafe,
    /// If `1` output is set to fail safe output
    EStop,
    /// The number of packets received by the serial device
    PacketIn,
    /// The number of packets sent by the serial device
    PacketOut,
    /// The number of received packets with invalid checksums
    CheckBad,
    /// The number of received packets with invalid data
    PacketBad,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum WriteRegister {
    /// The controller’s commanded throttle value
    Throttle,
    /// The E. Stop/RX Glitch fail safe output (0 = 1ms; 100 = 2ms)
    FailSafe,
    /// If `1` output is set to fail safe output
    EStop,
    /// The number of packets received by the serial device
    PacketIn,
    /// The number of packets sent by the serial device
    PacketOut,
    /// The number of received packets with invalid checksums
    CheckBad,
    /// The number of received packets with invalid data
    PacketBad,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct Reserved(u8);

#[derive(Debug, PartialEq, Eq)]
pub enum Command {
    ReadRegister(ReadRegister),
    WriteRegister(WriteRegister, CommandData),
    Reserved(Reserved, CommandData),
}

#[derive(Debug, PartialEq, Eq)]
pub struct CommandData(pub u16);

#[derive(Debug, PartialEq, Eq)]
pub struct ResponseData(pub u16);
