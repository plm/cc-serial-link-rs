use super::*;
use crate::protocol::{ttl::*, *};

use winnow::{
    binary::{le_u16, u8},
    combinator::trace,
    error::ParserError,
    stream::{AsBytes, Compare, Stream, StreamIsPartial},
    token::{literal, one_of, take},
    PResult, Parser,
};

type Output<O> = PResult<O>;

fn command_start<I>(i: &mut I) -> Output<DeviceID>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace(
        "command_start",
        u8.verify(|&v| v >= 0b10000000)
            .map(|v| v ^ 0b10000000)
            .try_map(DeviceID::try_from),
    )
    .parse_next(i)
}

fn register_address<I>(i: &mut I) -> Output<Register>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
{
    trace("register_address", u8.map(Register::from)).parse_next(i)
}

fn command_data<I>(i: &mut I) -> Output<CommandData>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
    <I as Stream>::Slice: AsBytes,
{
    trace("command_data", (le_u16).map(CommandData)).parse_next(i)
}

fn checksum<I, O, E>(mut parser: impl Parser<I, O, E>) -> impl Parser<I, O, E>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
    <I as Stream>::Slice: AsBytes,
    E: ParserError<I>,
{
    trace("checksum", move |i: &mut I| {
        let checkpoint = i.checkpoint();
        let remaining = i.eof_offset();
        let output = parser.parse_next(i)?;
        let consumed = remaining - i.eof_offset();
        i.reset(&checkpoint);
        let bytes = take(consumed).parse_next(i)?;
        let mut expected = 0_u8;
        for &byte in bytes.as_bytes() {
            expected = expected.wrapping_sub(byte);
        }
        let _ = one_of(expected).parse_next(i)?;
        Ok(output)
    })
}

fn ttl_serial_command<I>(i: &mut I) -> Output<TTLSerialCommand>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
    <I as Stream>::Slice: AsBytes,
{
    trace(
        "ttl_serial_command",
        checksum(move |i: &mut I| {
            let device = command_start(i)?;
            let address = register_address(i)?;
            let data = command_data(i)?;
            let command = match address {
                Register::Read(reg) => Command::ReadRegister(reg),
                Register::Write(reg) => Command::WriteRegister(reg, data),
                Register::Reserved(reg) => Command::Reserved(reg, data),
            };
            Ok(TTLSerialCommand::new(device, command))
        }),
    )
    .parse_next(i)
}

fn response_data<I>(i: &mut I) -> Output<ResponseData>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
    <I as Stream>::Slice: AsBytes,
{
    trace("response_data", le_u16.map(ResponseData)).parse_next(i)
}

fn ttl_serial_response<I>(i: &mut I) -> Output<TTLSerialResponse>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
    <I as Stream>::Slice: AsBytes,
{
    trace(
        "ttl_serial_response",
        checksum(move |i: &mut I| {
            let data = response_data(i)?;
            Ok(TTLSerialResponse::new(data))
        }),
    )
    .parse_next(i)
}

fn ttl_serial_clear<I>(i: &mut I) -> Output<TTLSerialClear>
where
    I: StreamIsPartial,
    I: Stream<Token = u8>,
    I: Compare<[u8; 5]>,
{
    trace("ttl_serial_response", move |i: &mut I| {
        let _ = literal(TTLSerialClear::RAW).parse_next(i)?;
        Ok(TTLSerialClear)
    })
    .parse_next(i)
}

impl PacketParser<Self> for TTLSerialCommand {
    fn parse_packet(input: Input) -> ParserResult<Self> {
        ttl_serial_command.parse(input).map_err(|_| ParserError)
    }
}

impl PacketParser<Self> for TTLSerialResponse {
    fn parse_packet(input: Input) -> ParserResult<Self> {
        ttl_serial_response.parse(input).map_err(|_| ParserError)
    }
}

impl PacketParser<Self> for TTLSerialClear {
    fn parse_packet(input: Input) -> ParserResult<Self> {
        ttl_serial_clear.parse(input).map_err(|_| ParserError)
    }
}
