use super::*;
use crate::sealed::Sealed;
use paste::paste;

pub trait Read: Sealed {
    fn read(&self) -> ReadRegister;
}

pub trait Write: Sealed {
    fn write(&self) -> WriteRegister;
}

impl Register {
    pub fn read(register: impl Read) -> Self {
        Self::Read(register.read())
    }

    pub fn write(register: impl Write) -> Self {
        Self::Write(register.write())
    }
}

macro_rules! read_register {
    ( $($t:ty)+ ) => {
        $(
        paste! {
        impl Sealed for $t {}

        impl Read for $t {
            fn read(&self) -> ReadRegister {
                ReadRegister::$t
            }
        }
        }
        )+
    }
}

macro_rules! write_register {
    ( $($t:ty)+ ) => {
        $(
        paste! {
        impl Write for $t {
            fn write(&self) -> WriteRegister {
                WriteRegister::$t
            }
        }
        }
        )+
    }
}

/// The controller's input voltage
pub struct Voltage;

/// The controller's input voltage ripple
pub struct Ripple;

/// The controller’s current draw
pub struct Current;

/// The controller’s commanded throttle value
pub struct Throttle;

/// The controller's output throttle percentage
pub struct Power;

/// The motor's electrical RPM
pub struct Speed;

/// The controller's temperature
pub struct Temp;

/// The BEC's voltage
pub struct BECVolt;

/// The BEC's current load
pub struct BECCurrent;

/// The raw NTC temperature value
pub struct RawNTC;

/// The raw linear temperature value
pub struct RawLinear;

/// Whether the serial device is in Link Live mode
pub struct LinkLive;

/// The E. Stop/RX Glitch fail safe output (0 = 1ms; 100 = 2ms)
pub struct FailSafe;

/// If `1` output is set to fail safe output
pub struct EStop;

/// The number of packets received by the serial device
pub struct PacketIn;

/// The number of packets sent by the serial device
pub struct PacketOut;

/// The number of received packets with invalid checksums
pub struct CheckBad;

/// The number of received packets with invalid data
pub struct PacketBad;

read_register! {
    Voltage
    Ripple
    Current
    Throttle
    Power
    Speed
    Temp
    BECVolt
    BECCurrent
    RawNTC
    RawLinear
    LinkLive
    FailSafe
    EStop
    PacketIn
    PacketOut
    CheckBad
    PacketBad
}

write_register! {
    Throttle
    FailSafe
    EStop
    PacketIn
    PacketOut
    CheckBad
    PacketBad
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn register_value_to_enum_conversion() {
        assert_eq!(Voltage.read(), ReadRegister::Voltage);
        assert_eq!(Ripple.read(), ReadRegister::Ripple);
        assert_eq!(Current.read(), ReadRegister::Current);
        assert_eq!(Throttle.read(), ReadRegister::Throttle);
        assert_eq!(Power.read(), ReadRegister::Power);
        assert_eq!(Speed.read(), ReadRegister::Speed);
        assert_eq!(Temp.read(), ReadRegister::Temp);
        assert_eq!(BECVolt.read(), ReadRegister::BECVolt);
        assert_eq!(BECCurrent.read(), ReadRegister::BECCurrent);
        assert_eq!(RawNTC.read(), ReadRegister::RawNTC);
        assert_eq!(RawLinear.read(), ReadRegister::RawLinear);
        assert_eq!(LinkLive.read(), ReadRegister::LinkLive);
        assert_eq!(FailSafe.read(), ReadRegister::FailSafe);
        assert_eq!(EStop.read(), ReadRegister::EStop);
        assert_eq!(PacketIn.read(), ReadRegister::PacketIn);
        assert_eq!(PacketOut.read(), ReadRegister::PacketOut);
        assert_eq!(CheckBad.read(), ReadRegister::CheckBad);
        assert_eq!(PacketBad.read(), ReadRegister::PacketBad);
        assert_eq!(Throttle.write(), WriteRegister::Throttle);
        assert_eq!(FailSafe.write(), WriteRegister::FailSafe);
        assert_eq!(EStop.write(), WriteRegister::EStop);
        assert_eq!(PacketIn.write(), WriteRegister::PacketIn);
        assert_eq!(PacketOut.write(), WriteRegister::PacketOut);
        assert_eq!(CheckBad.write(), WriteRegister::CheckBad);
        assert_eq!(PacketBad.write(), WriteRegister::PacketBad);
    }
}
