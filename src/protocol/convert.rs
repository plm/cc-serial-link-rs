use super::*;

impl From<ReadRegister> for u8 {
    fn from(register: ReadRegister) -> Self {
        match register {
            ReadRegister::Voltage => 0,
            ReadRegister::Ripple => 1,
            ReadRegister::Current => 2,
            ReadRegister::Throttle => 3,
            ReadRegister::Power => 4,
            ReadRegister::Speed => 5,
            ReadRegister::Temp => 6,
            ReadRegister::BECVolt => 7,
            ReadRegister::BECCurrent => 8,
            ReadRegister::RawNTC => 9,
            ReadRegister::RawLinear => 10,
            ReadRegister::LinkLive => 25,
            ReadRegister::FailSafe => 26,
            ReadRegister::EStop => 27,
            ReadRegister::PacketIn => 28,
            ReadRegister::PacketOut => 29,
            ReadRegister::CheckBad => 30,
            ReadRegister::PacketBad => 31,
        }
    }
}

impl From<WriteRegister> for u8 {
    fn from(register: WriteRegister) -> Self {
        match register {
            WriteRegister::Throttle => 128,
            WriteRegister::FailSafe => 129,
            WriteRegister::EStop => 130,
            WriteRegister::PacketIn => 131,
            WriteRegister::PacketOut => 132,
            WriteRegister::CheckBad => 133,
            WriteRegister::PacketBad => 134,
        }
    }
}

impl From<Register> for u8 {
    fn from(register: Register) -> Self {
        match register {
            Register::Read(read) => read.into(),
            Register::Write(write) => write.into(),
            Register::Reserved(Reserved(byte)) => byte,
        }
    }
}

impl From<u8> for Register {
    fn from(value: u8) -> Self {
        match value {
            0 => Self::Read(ReadRegister::Voltage),
            1 => Self::Read(ReadRegister::Ripple),
            2 => Self::Read(ReadRegister::Current),
            3 => Self::Read(ReadRegister::Throttle),
            4 => Self::Read(ReadRegister::Power),
            5 => Self::Read(ReadRegister::Speed),
            6 => Self::Read(ReadRegister::Temp),
            7 => Self::Read(ReadRegister::BECVolt),
            8 => Self::Read(ReadRegister::BECCurrent),
            9 => Self::Read(ReadRegister::RawNTC),
            10 => Self::Read(ReadRegister::RawLinear),
            25 => Self::Read(ReadRegister::LinkLive),
            26 => Self::Read(ReadRegister::FailSafe),
            27 => Self::Read(ReadRegister::EStop),
            28 => Self::Read(ReadRegister::PacketIn),
            29 => Self::Read(ReadRegister::PacketOut),
            30 => Self::Read(ReadRegister::CheckBad),
            31 => Self::Read(ReadRegister::PacketBad),
            128 => Self::Write(WriteRegister::Throttle),
            129 => Self::Write(WriteRegister::FailSafe),
            130 => Self::Write(WriteRegister::EStop),
            131 => Self::Write(WriteRegister::PacketIn),
            132 => Self::Write(WriteRegister::PacketOut),
            133 => Self::Write(WriteRegister::CheckBad),
            134 => Self::Write(WriteRegister::PacketBad),
            _ => Self::Reserved(Reserved(value)),
        }
    }
}

impl From<Reserved> for u8 {
    fn from(reserved: Reserved) -> Self {
        reserved.0
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn register_conversion() {
        assert_eq!(Register::from(0), Register::Read(ReadRegister::Voltage));
        assert_eq!(u8::from(Register::Read(ReadRegister::Voltage)), 0);
        assert_eq!(Register::from(1), Register::Read(ReadRegister::Ripple));
        assert_eq!(u8::from(Register::Read(ReadRegister::Ripple)), 1);
        assert_eq!(Register::from(2), Register::Read(ReadRegister::Current));
        assert_eq!(u8::from(Register::Read(ReadRegister::Current)), 2);
        assert_eq!(Register::from(3), Register::Read(ReadRegister::Throttle));
        assert_eq!(u8::from(Register::Read(ReadRegister::Throttle)), 3);
        assert_eq!(Register::from(4), Register::Read(ReadRegister::Power));
        assert_eq!(u8::from(Register::Read(ReadRegister::Power)), 4);
        assert_eq!(Register::from(5), Register::Read(ReadRegister::Speed));
        assert_eq!(u8::from(Register::Read(ReadRegister::Speed)), 5);
        assert_eq!(Register::from(6), Register::Read(ReadRegister::Temp));
        assert_eq!(u8::from(Register::Read(ReadRegister::Temp)), 6);
        assert_eq!(Register::from(7), Register::Read(ReadRegister::BECVolt));
        assert_eq!(u8::from(Register::Read(ReadRegister::BECVolt)), 7);
        assert_eq!(Register::from(8), Register::Read(ReadRegister::BECCurrent));
        assert_eq!(u8::from(Register::Read(ReadRegister::BECCurrent)), 8);
        assert_eq!(Register::from(9), Register::Read(ReadRegister::RawNTC));
        assert_eq!(u8::from(Register::Read(ReadRegister::RawNTC)), 9);
        assert_eq!(Register::from(10), Register::Read(ReadRegister::RawLinear));
        assert_eq!(u8::from(Register::Read(ReadRegister::RawLinear)), 10);
        assert_eq!(Register::from(25), Register::Read(ReadRegister::LinkLive));
        assert_eq!(u8::from(Register::Read(ReadRegister::LinkLive)), 25);
        assert_eq!(Register::from(26), Register::Read(ReadRegister::FailSafe));
        assert_eq!(u8::from(Register::Read(ReadRegister::FailSafe)), 26);
        assert_eq!(Register::from(27), Register::Read(ReadRegister::EStop));
        assert_eq!(u8::from(Register::Read(ReadRegister::EStop)), 27);
        assert_eq!(Register::from(28), Register::Read(ReadRegister::PacketIn));
        assert_eq!(u8::from(Register::Read(ReadRegister::PacketIn)), 28);
        assert_eq!(Register::from(29), Register::Read(ReadRegister::PacketOut));
        assert_eq!(u8::from(Register::Read(ReadRegister::PacketOut)), 29);
        assert_eq!(Register::from(30), Register::Read(ReadRegister::CheckBad));
        assert_eq!(u8::from(Register::Read(ReadRegister::CheckBad)), 30);
        assert_eq!(Register::from(31), Register::Read(ReadRegister::PacketBad));
        assert_eq!(u8::from(Register::Read(ReadRegister::PacketBad)), 31);
        assert_eq!(
            Register::from(128),
            Register::Write(WriteRegister::Throttle)
        );
        assert_eq!(u8::from(Register::Write(WriteRegister::Throttle)), 128);
        assert_eq!(
            Register::from(129),
            Register::Write(WriteRegister::FailSafe)
        );
        assert_eq!(u8::from(Register::Write(WriteRegister::FailSafe)), 129);
        assert_eq!(Register::from(130), Register::Write(WriteRegister::EStop));
        assert_eq!(u8::from(Register::Write(WriteRegister::EStop)), 130);
        assert_eq!(
            Register::from(131),
            Register::Write(WriteRegister::PacketIn)
        );
        assert_eq!(u8::from(Register::Write(WriteRegister::PacketIn)), 131);
        assert_eq!(
            Register::from(132),
            Register::Write(WriteRegister::PacketOut)
        );
        assert_eq!(u8::from(Register::Write(WriteRegister::PacketOut)), 132);
        assert_eq!(
            Register::from(133),
            Register::Write(WriteRegister::CheckBad)
        );
        assert_eq!(u8::from(Register::Write(WriteRegister::CheckBad)), 133);
        assert_eq!(
            Register::from(134),
            Register::Write(WriteRegister::PacketBad)
        );
        assert_eq!(u8::from(Register::Write(WriteRegister::PacketBad)), 134);
        for v in (32..=127).chain(135..=u8::MAX) {
            assert_eq!(Register::from(v), Register::Reserved(Reserved(v)));
            assert_eq!(u8::from(Register::Reserved(Reserved(v))), v);
        }
    }
}
