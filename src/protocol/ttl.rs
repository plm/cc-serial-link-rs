use super::*;

use core::fmt;

#[cfg(feature = "std")]
use std::error::Error;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct DeviceID(u8);

impl From<DeviceID> for u8 {
    fn from(device_id: DeviceID) -> Self {
        device_id.0
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct InvalidDeviceID(pub u8);

impl fmt::Display for InvalidDeviceID {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Invalid DeviceID {}", self.0)
    }
}

#[cfg(feature = "std")]
impl Error for InvalidDeviceID {}

impl TryFrom<u8> for DeviceID {
    type Error = InvalidDeviceID;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0..=63 => Ok(Self(value)),
            _ => Err(InvalidDeviceID(value)),
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct TTLSerialCommand {
    pub device: DeviceID,
    pub command: Command,
}

impl TTLSerialCommand {
    pub fn new(device: DeviceID, command: Command) -> Self {
        Self { device, command }
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct TTLSerialResponse {
    pub data: ResponseData,
}

impl TTLSerialResponse {
    pub fn new(data: ResponseData) -> Self {
        Self { data }
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct TTLSerialClear;

impl TTLSerialClear {
    pub const RAW: [u8; 5] = [0x0; 5];
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn device_id_conversion() {
        assert_eq!(u8::from(DeviceID(63)), 63);
        assert_eq!(DeviceID::try_from(1), Ok(DeviceID(1)));
        for v in 64..u8::MAX {
            assert_eq!(DeviceID::try_from(v), Err(InvalidDeviceID(v)));
        }
    }
}
