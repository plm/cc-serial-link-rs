use super::*;
use crate::protocol::{ttl::*, *};

const READ_COMMAND_DATA: [u8; 2] = [0x0; 2];

impl Serializer<Self> for TTLSerialCommand {
    fn serialize_packet<'b>(&self, buffer: Input<'b>) -> Output<'b> {
        if buffer.len() < 5 {
            Err(SerializerError)
        } else {
            buffer[0] = u8::from(self.device) | 0b10000000;
            let (register, data) = match &self.command {
                Command::ReadRegister(reg) => (u8::from(*reg), READ_COMMAND_DATA),
                Command::WriteRegister(reg, data) => (u8::from(*reg), data.0.to_le_bytes()),
                Command::Reserved(reg, data) => (u8::from(*reg), data.0.to_le_bytes()),
            };
            buffer[1] = register;
            buffer[2..=3].copy_from_slice(&data);
            let mut checksum = 0_u8;
            for &byte in &buffer[0..=3] {
                checksum = checksum.wrapping_sub(byte)
            }
            buffer[4] = checksum;
            Ok(&buffer[..5])
        }
    }
}

impl Serializer<Self> for TTLSerialResponse {
    fn serialize_packet<'b>(&self, buffer: Input<'b>) -> Output<'b> {
        if buffer.len() < 3 {
            Err(SerializerError)
        } else {
            buffer[0..=1].copy_from_slice(&self.data.0.to_le_bytes());
            let mut checksum = 0_u8;
            for &byte in &buffer[0..=1] {
                checksum = checksum.wrapping_sub(byte)
            }
            buffer[2] = checksum;
            Ok(&buffer[..3])
        }
    }
}

impl Serializer<Self> for TTLSerialClear {
    fn serialize_packet<'b>(&self, buffer: Input<'b>) -> Output<'b> {
        if buffer.len() < 5 {
            Err(SerializerError)
        } else {
            buffer[..5].copy_from_slice(&Self::RAW);
            Ok(&buffer[..5])
        }
    }
}
