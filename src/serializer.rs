pub mod ttl;

#[derive(Debug, PartialEq, Eq)]
pub struct SerializerError;

pub type Input<'i> = &'i mut [u8];
pub type Output<'o> = Result<&'o [u8], SerializerError>;

pub trait Serializer<P> {
    fn serialize_packet<'b>(&self, buffer: Input<'b>) -> Output<'b>;
}
