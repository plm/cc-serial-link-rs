Castle Creations Serial Link in Rust
===

An implementation of [Castle Creations' Serial Link 1.5 protocol](https://www.castlecreations.com/en/serial-link-010-0121-00).

License
---

Licensed under either of

* Apache License, Version 2.0
  ([LICENSE-APACHE](LICENSE-APACHE))
* MIT license
  ([LICENSE-MIT](LICENSE-MIT))

at your option.

Contribution
---

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
